package com.attinad.Threads;

import com.attinad.*;
import com.attinad.DBhandlers.DbOperations;
import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;
import com.attinad.interfaces.IOperations;

import java.io.IOException;
import java.util.ArrayList;

public class DbManager implements Runnable, IOperations {
    private ArrayList<String> fileNameList;
    private boolean bExitFileProcessing;

    public DbManager() throws InvalidApplicationSetupException, DataSourceException {
        listCurrentiles();
    }

    public void listCurrentiles() throws InvalidApplicationSetupException, DataSourceException {

        DbOperations dbOperations = new DbOperations();
        fileNameList = dbOperations.readFromDB();

    }

    @Override
    public void run() {
       try {
            monitor();
        } catch (IOException e) {
            System.err.println("unable to input output operaitons");
        } catch (InvalidApplicationSetupException e) {
            System.err.println("Driver problems please contact application provider");
        } catch (DataSourceException e) {
            System.out.println("IOexception");
        }
    }

    @Override
    public void monitor() throws IOException, InvalidApplicationSetupException, DataSourceException {
        ArrayList<String> getFileNameList = new ArrayList<String>();
        FileDetails fileDetails = new FileDetails();
        DbOperations dbOperations = new DbOperations();
        while (!bExitFileProcessing) {
            // repeat of variable declared in constructor. But if you want to keep the scope local, this declaration is fine
           
            try {
                getFileNameList = dbOperations.readFromDB();
                for (String f : getFileNameList) {
                    if (!fileNameList.contains(f)) {

                        fileDetails = dbOperations.readOneFile(f);
                        fileNameList.add(f);
                        write(fileDetails);
                    }
                }
            } catch (DataSourceException e) {
                e.printStackTrace();
            } catch (InvalidApplicationSetupException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void write(FileDetails file) throws InvalidApplicationSetupException, DataSourceException {

        file.display();

    }


}
