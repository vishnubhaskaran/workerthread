package com.attinad.Threads;

import com.attinad.*;
import com.attinad.DBhandlers.DbOperations;
import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;
import com.attinad.interfaces.IOperations;

import java.io.*;
import java.util.ArrayList;

public class FileManager implements Runnable, IOperations {
    FileDetails fileDetails;
    private ArrayList<String> fileNameList;
    private boolean bExitFileProcessing;

   public FileManager() throws InvalidApplicationSetupException, DataSourceException {
        listCurrentiles();
    }

    public void listCurrentiles() throws InvalidApplicationSetupException, DataSourceException {

        DbOperations dbOperations = new DbOperations();
        fileNameList = dbOperations.readFromDB();

    }


    @Override
    public void run() {

        try {
            try {
                monitor();
            } catch (InvalidApplicationSetupException e) {
                System.err.println("Driver problems please contact application provider");
            } catch (DataSourceException e) {
               System.out.println("Data source exception");
            }
        } catch (IOException e) {
            System.out.println("IOexception");
        }

    }

    @Override
    public void monitor() throws IOException, InvalidApplicationSetupException, DataSourceException {
        fileDetails = new FileDetails();
        String filename;
        while (!bExitFileProcessing) {
            File file = new File("E:/MyFolder");
            File[] files = file.listFiles();
            for (File f : files) {
                filename = nameFilter(f);

                if (!fileNameList.contains(filename)) {
                    fileDetails.setFileContent(readFromFile(f));
                    fileDetails.setFileSize((int) f.length());
                    fileDetails.setFileName(filename);
                    write(fileDetails);
                }

            }
        }
    }

    @Override
    public void write(FileDetails fileDetails) throws InvalidApplicationSetupException, DataSourceException {

        DbOperations dbinsert = new DbOperations();
        dbinsert.insert(fileDetails);
        //dbinsert.insert("vsihnu", "sadasd", 12);
        //System.out.println("name" + fileName);
        fileNameList.add(fileDetails.getFileName());

    }

    public String nameFilter(File file) {
        String filename = null;
        if (file.getName().contains(".txt")) {
            filename = file.getName().replace(".txt", "");
        }
        return filename;
    }

    public String readFromFile(File file) throws IOException {
        String content;
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        content = bufferedReader.readLine();
        return content;
    }


}
