package com.attinad.DBhandlers;

import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnect {
    private static Connection con;

    public static synchronized Connection connect() throws DataSourceException, InvalidApplicationSetupException {
        if (con != null) {
            return con;
        }
        try {

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/locations?autoReconnect=true&useSSL=false", "root", "Neethu123!@#");
        } catch (SQLException e) {
            throw new DataSourceException(e.getMessage());
        } catch (ClassNotFoundException cnfe) {
            throw new InvalidApplicationSetupException("Driver not configured properly");
        }
        return con;
    }

    public void closeConnection() {
        try {
            con.close();
        } catch (SQLException e) {
            //Not propogating
        }
    }

}
