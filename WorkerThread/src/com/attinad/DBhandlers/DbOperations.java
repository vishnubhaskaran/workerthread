package com.attinad.DBhandlers;

import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;
import com.attinad.FileDetails;

import java.sql.*;
import java.util.ArrayList;

public class DbOperations {

    private boolean bExitFileProcessing;

    public void insert(FileDetails file) throws InvalidApplicationSetupException, DataSourceException {
        //  System.out.println(name);
        DBconnect dBconnect = new DBconnect();
        Connection connection;

        String query;
        query = "INSERT into  filedetails VALUES ('" + file.getFileName() + "','" + file.getFileContent() + "'," + "'" + file.getFileSize() + "')";
        try {
            connection = dBconnect.connect();
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.execute();
        } catch (SQLException e) {
            throw new DataSourceException(e.getMessage());
        }
    }

    public ArrayList<String> readFromDB() throws InvalidApplicationSetupException, DataSourceException {

        ArrayList<String> arrayList = new ArrayList<String>();
        Statement st;
        ResultSet rs;
        String query;
        DBconnect dBconnect = new DBconnect();
        Connection connection;

        query = "SELECT fileName  from filedetails";
        try {
            connection = dBconnect.connect();
            st = connection.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                arrayList.add(rs.getString("fileName"));
            }
        } catch (SQLException e) {
            throw new DataSourceException(e.getMessage());
        }
        return arrayList;
    }

    public FileDetails readOneFile(String name) throws InvalidApplicationSetupException, DataSourceException {
        Statement st;
        ResultSet rs;
        String query;
        DBconnect dBconnect = new DBconnect();
        Connection connection;
        FileDetails fileDetails = new FileDetails();
        query = String.format("SELECT fileName,content,size from filedetails where fileName='%s'", name);

        try {
            connection = dBconnect.connect();
            st = connection.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {

                fileDetails.setFileName(rs.getString("fileName"));
                fileDetails.setFileContent(rs.getString("content"));
                fileDetails.setFileSize(rs.getInt("size"));

            }
        } catch (SQLException e) {
            throw new DataSourceException(e.getMessage());
        }


        return fileDetails;
    }


}
