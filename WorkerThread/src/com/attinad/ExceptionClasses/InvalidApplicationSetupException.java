package com.attinad.ExceptionClasses;

public class InvalidApplicationSetupException extends Exception {

    public InvalidApplicationSetupException(String message) {
        super(message);
    }
}
