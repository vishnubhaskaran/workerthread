package com.attinad.ExceptionClasses;

public class DataSourceException extends Exception {
    public DataSourceException(String message) {
        super(message);
    }
}
