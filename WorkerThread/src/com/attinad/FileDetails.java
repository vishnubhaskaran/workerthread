package com.attinad;

public class FileDetails {

    private String fileName;
    private int fileSize;
    private String fileContent;

    public FileDetails(String fileName, int fileSize, String fileContent) {
        this.fileName = fileName;
        this.fileContent = fileContent;
        this.fileSize = fileSize;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public FileDetails() {
    }

    public String getFileContent() {
        return fileContent;
    }

    public int getFileSize() {

        return fileSize;
    }

    public String getFileName() {

        return fileName;
    }

    public void display() {
        System.out.println(this.fileContent);
        System.out.println(this.fileName);
        System.out.println(this.fileSize);


    }
}
