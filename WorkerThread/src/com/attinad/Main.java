package com.attinad;

import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;
import com.attinad.Threads.DbManager;
import com.attinad.Threads.FileManager;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        // write your code here

        FileManager fileOperations = null;
        DbManager dbManager = null;
        try {

            fileOperations = new FileManager();
            dbManager = new DbManager();

        } catch (InvalidApplicationSetupException e) {
            System.err.println("application not configured");
        } catch (DataSourceException e) {
            System.err.println("Data Source error");
        }
        Thread thread = new Thread(fileOperations);
        Thread thread1 = new Thread(dbManager);
        thread.start();
        thread1.start();
        thread1.join();

    }
}
