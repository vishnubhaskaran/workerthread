package com.attinad.interfaces;

import com.attinad.ExceptionClasses.DataSourceException;
import com.attinad.FileDetails;
import com.attinad.ExceptionClasses.InvalidApplicationSetupException;

import java.io.IOException;

public interface IOperations {

    public void monitor() throws IOException, InvalidApplicationSetupException, DataSourceException;
    public void write(FileDetails fileDetails) throws InvalidApplicationSetupException, DataSourceException;

}
